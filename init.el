(require 'package)
(setq package-enable-at-startup nil)

(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org"))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))
(put 'narrow-to-region 'disabled nil)
(put 'set-goal-column 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#3F3F3F" "#CC9393" "#7F9F7F" "#F0DFAF" "#8CD0D3" "#DC8CC3" "#93E0E3" "#DCDCCC"])
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(custom-safe-themes
   (quote
    ("c82d24bfba431e8104219bfd8e90d47f1ad6b80a504a7900cbee002a8f04392f" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" default)))
 '(erc-autojoin-channels-alist
   (quote
    (("freenode.net" "#emacs-beginners" "#bash" "#bitcoin" "#emacs" "#latex" "#org-mode"))))
 '(erc-autojoin-timing (quote ident))
 '(erc-fill-function (quote erc-fill-static))
 '(erc-fill-static-center 22)
 '(erc-hide-list (quote ("JOIN" "PART" "QUIT")))
 '(erc-lurker-hide-list (quote ("JOIN" "PART" "QUIT")))
 '(erc-prompt-for-nickserv-password nil)
 '(erc-server-reconnect-attempts 5)
 '(erc-server-reconnect-timeout 3)
 '(ess-help-own-frame t)
 '(fci-rule-color "#383838")
 '(matlab-shell-command-switches (quote ("-nodesktop -nosplash")))
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(org-agenda-files (quote ("~/mylife.org")))
 '(org-catch-invisible-edits (quote show-and-error))
 '(org-confirm-babel-evaluate nil)
 '(org-export-with-section-numbers nil)
 '(org-hide-emphasis-markers t)
 '(org-highlight-latex-and-related (quote (latex entities)))
 '(org-journal-dir "~/journal/")
 '(org-pretty-entities t)
 '(org-startup-folded t)
 '(org-startup-indented t)
 '(org-startup-with-inline-images t)
 '(package-selected-packages
   (quote
    (ox-hugo pretty-mode subatomic-theme jeison powerthesaurus org-capture ein whole-line-or-region smart-mode-line-powerline-theme gitlab git monokai-theme oer-reveal org-re-reveal-ref org-re-reveal ox-reveal csv-mode treemacs-magit treemacs-icons-dired treemacs-projectile treemacs-evil treemacs magit lv auctex-latexmk auctex auto-package-update latex-preview-pane zenburn-theme which-key web-mode use-package undo-tree try synosaurus stan-snippets poly-R org-pdfview org-journal org-bullets org-ac noflet multiple-cursors moe-theme julia-mode jedi irony-eldoc iedit hydra hungry-delete htmlize git-timemachine git-gutter ggtags flycheck fill-column-indicator expand-region ess-view elpy dash-functional counsel company-math company-jedi company-irony color-theme-modern color-theme beacon base16-theme auto-complete-auctex ample-theme aggressive-indent adaptive-wrap ace-window ac-ispell)))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3"))
